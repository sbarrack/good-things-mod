package com.stephenbarrack.goodthings.mixin;

import net.minecraft.server.world.ServerWorld;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.Constant;
import org.spongepowered.asm.mixin.injection.ModifyConstant;

/**
 * Sets the world to the correct time if players sleep through the day
 */
@Mixin(value = ServerWorld.class, priority = 5000)
public class NightOwlTimeMixin {

    /**
     *
     * @param l 24000L; <i>one Minecraft day in game ticks</i>
     * @return 12000L; <i>half a Minecraft day in game ticks</i>
     */
    @ModifyConstant(method = "tick(Ljava/util/function/BooleanSupplier;)V", constant = @Constant(longValue = 24000L))
    private long halfDay(long l) {
        return 12000L;
    }
}
