package com.stephenbarrack.goodthings.mixin;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ai.RangedAttackMob;
import net.minecraft.entity.ai.goal.BowAttackGoal;
import net.minecraft.entity.mob.HostileEntity;
import net.minecraft.entity.projectile.ProjectileUtil;
import net.minecraft.item.BowItem;
import net.minecraft.item.Items;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(value = BowAttackGoal.class, priority = 5000)
public class SkeletonMixin<T extends HostileEntity & RangedAttackMob> {
	@Shadow
	private T actor;

	private final int tps = 20;
	private final int attackInterval = 1 * tps;
	private final int attentionSpan = 3 * -tps;
	private final float chanceChangeDirection = 0.3F;
	private final float maxStrafeSpeed = 0.5F;
	private final float minStrafeSpeed = maxStrafeSpeed * 1.0F;
	private final float trackingSpeed = 30.0F;
	private final double runningSpeed = 1.0D;
	private final double squaredRange = 15.0D;
	private final double approachDistance = squaredRange * 0.75F;
	private final double retreatDistance = squaredRange * 0.5F;

	private boolean isMovingLeft;
	private boolean isMovingBackward;
	private int combatTicks = -1;
	private int cooldown = -1;
	private int targetSeeingTicker;

	@Overwrite
	public void tick() {
		final LivingEntity target = actor.getTarget();
		if (target == null)
			return;
		final double targetSquaredDistance = actor.squaredDistanceTo(target.getX(), target.getY(), target.getZ());
		final boolean hasLineOfSight = actor.getVisibilityCache().canSee(target);

		// TODO Figure out AI

		if (hasLineOfSight != targetSeeingTicker > 0) {
			targetSeeingTicker = 0;
		}

		if (hasLineOfSight) {
			++targetSeeingTicker;
		} else {
			--targetSeeingTicker;
		}

		if (!(targetSquaredDistance > squaredRange) && targetSeeingTicker >= tps) {
			actor.getNavigation().stop();
			++combatTicks;
		} else {
			actor.getNavigation().startMovingTo(target, runningSpeed);
			combatTicks = -1;
		}

		if (combatTicks >= tps) {
			if (actor.getRandom().nextFloat() < chanceChangeDirection) {
				isMovingLeft = !isMovingLeft;
			}

			if (actor.getRandom().nextFloat() < chanceChangeDirection) {
				isMovingBackward = !isMovingBackward;
			}

			combatTicks = 0;
		}

		if (combatTicks > -1) {
			float strafeSpeed = actor.isUsingItem() ? minStrafeSpeed : maxStrafeSpeed;

			if (targetSquaredDistance > approachDistance) {
				isMovingBackward = false;
			} else if (targetSquaredDistance < retreatDistance) {
				isMovingBackward = true;
			}

			actor.getMoveControl().strafeTo(isMovingBackward ? -strafeSpeed : strafeSpeed,
					isMovingLeft ? strafeSpeed : -strafeSpeed);
			actor.lookAtEntity(target, trackingSpeed, trackingSpeed);
		} else {
			actor.getLookControl().lookAt(target, trackingSpeed, trackingSpeed);
		}

		if (actor.isUsingItem()) {
			if (hasLineOfSight) {
				final int i = actor.getItemUseTime();

				if (i >= tps) {
					actor.clearActiveItem();
					((RangedAttackMob) actor).attack(target, BowItem.getPullProgress(i));
					cooldown = attackInterval;
				}
			} else if (targetSeeingTicker < attentionSpan) {
				actor.clearActiveItem();
			}
		} else if (--cooldown <= 0 && targetSeeingTicker >= attentionSpan) {
			actor.setCurrentHand(ProjectileUtil.getHandPossiblyHolding(actor, Items.BOW));
		}
	}
}
