package com.stephenbarrack.goodthings.mixin;

import com.mojang.authlib.GameProfile;
import com.mojang.datafixers.util.Either;

import java.util.List;

import net.minecraft.advancement.criterion.Criteria;
import net.minecraft.entity.mob.HostileEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.encryption.PlayerPublicKey;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.stat.Stats;
import net.minecraft.text.Text;
import net.minecraft.util.Unit;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Box;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

/**
 * Allows players to sleep during the day
 */
@Mixin(value = ServerPlayerEntity.class, priority = 5000)
public abstract class NightOwlMixin extends PlayerEntity {

    @Shadow
    public abstract boolean isCreative();

    @Shadow
    public abstract void sendMessage(Text message, boolean actionBar);

    public NightOwlMixin(World w, BlockPos b, float f, GameProfile p, PlayerPublicKey k) {
        super(w, b, f, p, k);
    }

    /**
     * If the player tried to sleep and the game said, "No, because it's daytime,"
     * tries sleeping anyways
     *
     * @param pos Block position of the player trying to sleep
     * @param cir CallbackInfoReturnable
     */
    @Inject(method = "trySleep(Lnet/minecraft/util/math/BlockPos;)Lcom/mojang/datafixers/util/Either;", at = @At(value = "RETURN"), cancellable = true)
    private void tryDaySleep(BlockPos pos, CallbackInfoReturnable<Either<SleepFailureReason, Unit>> cir) {
        Either<SleepFailureReason, Unit> sillyReason = cir.getReturnValue();
        if (sillyReason.left().isEmpty()) {
            if (cir.isCancellable())
                cir.cancel();
            return;
        }
        if (sillyReason.left().get() != SleepFailureReason.NOT_POSSIBLE_NOW) {
            if (cir.isCancellable())
                cir.cancel();
            return;
        }

        if (!isCreative()) {
            final double horizRadius = 8.0D;
            final double vertRadius = 5.0D;
            Vec3d v = Vec3d.ofBottomCenter(pos);
            List<HostileEntity> list = world.getEntitiesByClass(HostileEntity.class,
                    new Box(v.getX() - horizRadius, v.getY() - vertRadius, v.getZ() - horizRadius,
                            v.getX() + horizRadius, v.getY() + vertRadius, v.getZ() + horizRadius),
                    (hostileEntity) -> {
                        return hostileEntity.isAngryAt(this);
                    });
            if (!list.isEmpty()) {
                cir.setReturnValue(Either.left(SleepFailureReason.NOT_SAFE));
                if (cir.isCancellable())
                    cir.cancel();
                return;
            }
        }

        Either<SleepFailureReason, Unit> sleep = super.trySleep(pos).ifRight((unit) -> {
            incrementStat(Stats.SLEEP_IN_BED);
            Criteria.SLEPT_IN_BED.trigger((ServerPlayerEntity)(Object)this);
        });
        if (!((ServerWorld) world).isSleepingEnabled()) {
            sendMessage(Text.translatable("sleep.not_possible"), true);
        }
        ((ServerWorld) world).updateSleepingPlayers();

        cir.setReturnValue(sleep);
        if (cir.isCancellable())
            cir.cancel();
        return;
    }
}
