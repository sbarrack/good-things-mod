package com.stephenbarrack.goodthings.mixin;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Objects;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.EnderChestInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.server.network.ServerPlayerEntity;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import com.stephenbarrack.goodthings.GoodThings;

@Mixin(value = ServerPlayerEntity.class, priority = 5000)
public class SoulboundMixin {
	public final int soulStrength = 10;
	private final Logger log = LogManager.getLogger();

	public SoulboundMixin() {
		log.info("Instantiated Soulbound mixin.");
	}

	// TODO Figure out why binding happens ~16 times for my one player (on server only?) or if it's just the laggy debugger

	@Inject(method = "onDeath", at = @At("INVOKE"))
	public void beforeDeath(DamageSource source, CallbackInfo callbackInfo) {
		ServerPlayerEntity player = (ServerPlayerEntity) (Object) this;
		PlayerInventory inventory = player.getInventory();
		EnderChestInventory enderChest = player.getEnderChestInventory();

		log.info(player.getName() + " is about to die...");

		// if (there is no existing soulbound inventory)
		if (!GoodThings.soulboundInventory.containsKey(player.getUuid())) {
			// make one
			log.info(player.getName() + " does not have a preexisting soulbound inventory");
			GoodThings.soulboundInventory.put(player.getUuid(), new Hashtable<>());
		}

		log.info("Looting the corpse...");
		for (int i = 0; i < inventory.size(); i++) {
			ItemStack item = inventory.getStack(i);

			log.info("Found item #" + i + " " + item.getName() + "...");

			if (item.getNbt() != null) {
				log.info(item.getName() + " has NBT");

				if (item.getNbt().get("Enchantments") != null) {
					log.info(item.getName() + " has enchantments");

					if (Objects.requireNonNull(item.getNbt().get("Enchantments")).asString()
							.contains("goodthings:soulbound")) {
						int damage = item.getDamage();
						int durability = item.getMaxDamage() - damage;

						log.info(item.getName() + " has soulbound");

						// if (strong enough to bind to soul)
						if (durability > item.getMaxDamage() / soulStrength || player.isCreative()
								|| player.isSpectator()) {
							// bind to soul
							GoodThings.soulboundInventory.get(player.getUuid()).put(i, item);
							inventory.removeStack(i);
							log.info(item.getName() + " successfully bound to " + player.getName());
						} // else if (player's enderchest has room)
						else if (enderChest.canInsert(item)) {
							// bind to enderchest
							enderChest.addStack(item);
							inventory.removeStack(i);
							log.info(item.getName() + " bound to ender chest");
						} else {
							log.info(item.getName() + " could not be bound to soul");
						}
					} else {
						log.info(item.getName() + " does not have soulbound");
					}
				} else {
					log.info(item.getName() + " does not have enchantments");
				}
			} else {
				log.info(item.getName() + " does not have NBT");
			}
		}
	}

	@Inject(method = "onDeath", at = @At("RETURN"))
	public void afterDeath(DamageSource source, CallbackInfo callbackInfo) {
		ServerPlayerEntity player = (ServerPlayerEntity) (Object) this;
		PlayerInventory inventory = player.getInventory();
		Enumeration<Integer> keys = GoodThings.soulboundInventory.get(player.getUuid()).keys();

		log.info("RIP" + player.getName() + "...");

		while (keys.hasMoreElements()) {
			int i = keys.nextElement();
			inventory.setStack(i, GoodThings.soulboundInventory.get(player.getUuid()).get(i));

			int damage = inventory.getStack(i).getDamage();
			int durability = inventory.getStack(i).getMaxDamage() - damage;
			if (durability > inventory.getStack(i).getMaxDamage() / soulStrength
					&& !(player.isCreative() || player.isSpectator())) {
				inventory.getStack(i).setDamage(damage + 100);
			}

			inventory.setStack(i, GoodThings.soulboundInventory.get(player.getUuid()).get(i));
		}

		GoodThings.soulboundInventory.get(player.getUuid()).clear();
	}

	@Inject(method = "copyFrom", at = @At("INVOKE"))
	private void onRespawn(ServerPlayerEntity oldPlayer, boolean alive, CallbackInfo ci) {
		ServerPlayerEntity player = (ServerPlayerEntity) (Object) this;

		log.info(player.getName() + " respawned and their inventory hopefully got cloned");
		player.getInventory().clone(oldPlayer.getInventory());
	}
}
