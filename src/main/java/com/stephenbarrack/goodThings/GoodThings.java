package com.stephenbarrack.goodthings;

import java.util.Hashtable;
import java.util.UUID;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.quiltmc.loader.api.ModContainer;
import org.quiltmc.qsl.base.api.entrypoint.ModInitializer;

import com.stephenbarrack.goodthings.enchantments.Soulbound;

public class GoodThings implements ModInitializer {
    public static Hashtable<UUID, Hashtable<Integer, ItemStack>> soulboundInventory;
    private final Logger log = LogManager.getLogger();

    /*
     * Ideas:
     * - Individual sound sliders or better sound control in general
     * - Toggles for individual mob griefing
     * - Mailboxes
     */

    @Override
    public void onInitialize(ModContainer mod) {
        log.info("Initializing...");

        soulboundInventory = new Hashtable<>();
        Registry.register(Registry.ENCHANTMENT, new Identifier("goodthings", "soulbound"),
                new Soulbound(Enchantment.Rarity.UNCOMMON, EnchantmentTarget.BREAKABLE, EquipmentSlot.values()));

        log.info("Initializing complete.");
    }
}
