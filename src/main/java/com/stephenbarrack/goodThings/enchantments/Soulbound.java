package com.stephenbarrack.goodthings.enchantments;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.text.MutableText;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class Soulbound extends Enchantment {
    private final Logger log = LogManager.getLogger();

    public Soulbound(Rarity rarity, EnchantmentTarget target, EquipmentSlot[] slots) {
        super(rarity, target, slots);
    }

    @Override
    public Text getName(int level) {
        MutableText name = Text.translatable(this.getTranslationKey()).formatted(Formatting.GREEN);

        if (level != 1) {
            MutableText lvl = Text.literal(" LVL INVALID").formatted(Formatting.DARK_RED);

            name.append(lvl);
            log.error(level + " is an invalid level for Soulbound!");
        }

        return name;
    }

    @Override
    protected boolean canAccept(Enchantment other) {
        return !(other.isCursed() || this == other);
    }

    @Override
    public boolean isTreasure() {
        return true;
    }
}
