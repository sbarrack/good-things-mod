# Good Things Mod

A Minecraft modification which aims to improve UX without changing core game mechanics. All good things come to those who wait.

## Useful documentation

- [Mixins](https://github.com/SpongePowered/Mixin/wiki)
    - [Cheatsheet](https://github.com/2xsaiko/mixin-cheatsheet)
    - [Injects ref](https://fabricmc.net/wiki/tutorial:mixin_injects)
    - [Examples](https://fabricmc.net/wiki/tutorial:mixin_examples)
    - [Javadoc](https://jenkins.liteloader.com/job/Mixin/javadoc/)
    - [Partial target selectors ref](https://jenkins.liteloader.com/job/Mixin/javadoc/org/spongepowered/asm/mixin/injection/struct/MemberInfo.html)
- [Fabric](https://fabricmc.net/wiki/start)
- [Quilt](https://forum.quiltmc.org/)
- [Modrinth](https://modrinth.com/)
